﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using ICities;
using Runaurufu.Utility;
using System.Xml.Serialization;
using System.Xml;

namespace Runaurufu.Console
{
  public class ConsoleSerializableData : SerializableDataExtensionBase
  {

    private ISerializableData serializableData;

    public override void OnCreated(ISerializableData serializableData)
    {
      //base.OnCreated(serializableData);

      this.serializableData = serializableData;
    }

    public override void OnLoadData()
    {
      //base.OnLoadData();

    }

    public override void OnSaveData()
    {
      //base.OnSaveData();

      
    }

    public static byte[] Serialize(object item)
    {
      BinaryFormatter binaryFormatter = new BinaryFormatter();
      using (MemoryStream memoryStream = new MemoryStream())
      {
        try
        {
          binaryFormatter.Serialize(memoryStream, item);
          memoryStream.Position = 0L;
          return memoryStream.ToArray();
        }
        catch (Exception ex)
        {
          Logger.Log("Serialize Exception:" + Environment.NewLine + ex.ToString());
        }
        return null;
      }
    }

    public static T Deserialize<T>(byte[] data) where T : class
    {
      BinaryFormatter binaryFormatter = new BinaryFormatter();
      using (MemoryStream memoryStream = new MemoryStream())
      {
        memoryStream.Write(data, 0, data.Length);
        memoryStream.Position = 0L;
        try
        {
          return binaryFormatter.Deserialize(memoryStream) as T;
        }
        catch (Exception ex)
        {
          Logger.Log("Deserialize Exception:" + Environment.NewLine + ex.ToString());
        }
        return null;
      }
    }

    public static byte[] XmlSerialize(object item)
    {
      if (item == null)
        return null;

      using (MemoryStream memoryStream = new MemoryStream())
      {
        try
        {
          XmlSerializer serializer = new XmlSerializer(item.GetType());
          using (XmlWriter xmlWriter = XmlWriter.Create(memoryStream))
          {
            serializer.Serialize(xmlWriter, item);
          }

          memoryStream.Position = 0L;
          return memoryStream.ToArray();
        }
        catch (Exception ex)
        {
          Logger.Log("XmlSerialize Exception:" + Environment.NewLine + ex.ToString());
        }
        return null;
      }
    }

    public static T XmlDeserialize<T>(byte[] data) where T : class
    {
      try
      {
        using (MemoryStream memoryStream = new MemoryStream())
        {
          memoryStream.Write(data, 0, data.Length);
          memoryStream.Position = 0L;

          XmlSerializer serializer = new XmlSerializer(typeof(T));
          using (XmlReader xmlReader = XmlReader.Create(memoryStream))
          {
            return (T)serializer.Deserialize(xmlReader);
          }
        }
      }
      catch (Exception ex)
      {
        Logger.Log("XmlDeserialize Exception:" + Environment.NewLine + ex.ToString());
        return null;
      }
    }
  }
}