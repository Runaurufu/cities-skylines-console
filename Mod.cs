﻿using System;
using System.Linq;
using ICities;
using UnityEngine.Networking;
using Runaurufu.Utility;
using Runaurufu.Common;
using Runaurufu.Console.UI;
using ColossalFramework.UI;

namespace Runaurufu.Console
{
  public class Mod : IUserMod
  {
    private static Mod instance;

    public static Mod GetInstance()
    {
      return instance;
    }

    public Mod()
    {
      instance = this;
    }

    public string Name
    {
      get
      {
        return "Console";
      }
    }

    public string Description
    {
      get
      {
        return "Extensible console for everyone!";
      }
    }

    public void OnSettingsUI(UIHelperBase helper)
    {
      ModConfig.LoadConfig();
      UIHelperBase helperBase = helper.AddGroup("Main options");

      ////helperBase.AddCheckbox("Dynamically Create Water Sources", ModConfig.GetInstance().DynamicWaterSourcesPlacement, ModSettings.DynamicWaterSourcesPlacementOnCheckChanged);
      ////helperBase.AddCheckbox("Spawn Additional Creeks", ModConfig.GetInstance().SpawnExtraCreeks, ModSettings.SpawnExtraCreeksOnCheckChanged);
      //helperBase.AddCheckbox("Weather Affect Water Sources", ModConfig.GetInstance().WeatherAffectsWaterSources, ModSettings.WeatherAffectsWaterSourcesOnCheckChanged);
      ////helperBase.AddCheckbox("Flowing Water Causes Erosion", ModConfig.GetInstance().FlowingWaterCausesErosion, ModSettings.FlowingWaterCausesErosionOnCheckChanged);
      //helperBase.AddCheckbox("Reduce Water Sources Intake", ModConfig.GetInstance().ReduceWaterSourcesIntake, ModSettings.ReduceWaterSourcesIntakeOnCheckChanged);
      //helperBase.AddCheckbox("Allow Floods", ModConfig.GetInstance().AllowFloods, ModSettings.AllowFloodsOnCheckChanged);
      //helperBase.AddCheckbox("Allow Droughts", ModConfig.GetInstance().AllowDroughts, ModSettings.AllowDroughtsOnCheckChanged);

      ////helperBase = helper.AddGroup("Climate Control Integration");
      ////helperBase.AddCheckbox("Use Climate Control Water Quantities", ModConfig.GetInstance().UseClimateControlWaterQuantities, ModSettings.UseClimateControlWaterQuantitiesOnCheckChanged);

      //helperBase = helper.AddGroup("Maintenance");
      //helperBase.AddButton("Reset Water Sources", ModSettings.ResetWaterSourcesOnButtonClicked);
      //helperBase.AddButton("Choose Map Preset", ModSettings.ChooseMapPresetOnButtonClicked);
    }

    internal static partial class ModSettings
    {
      //public static void UseClimateControlWaterQuantitiesOnCheckChanged(bool newValue)
      //{
      //  ModConfig.GetInstance().UseClimateControlWaterQuantities = newValue;


      //  IClimateControlEngine climateControlEngine = SingletonHelper.GetSingleInstance<IClimateControlEngine>();
      //  if (climateControlEngine != null)
      //  {
      //    if (ModConfig.GetInstance().UseClimateControlWaterQuantities == true)
      //      climateControlEngine.RegisterModuleHandler(ClimateControlModuleTypeIdentifiers.WaterSource, Mod.GetInstance());
      //    else
      //      climateControlEngine.DeregisterModuleHandler(ClimateControlModuleTypeIdentifiers.WaterSource, Mod.GetInstance());
      //  }

      //  ModConfig.SaveConfig();
      //}

      //public static void DynamicWaterSourcesPlacementOnCheckChanged(bool newValue)
      //{
      //  ModConfig.GetInstance().DynamicWaterSourcesPlacement = newValue;
      //  ModConfig.SaveConfig();
      //}

      //public static void SpawnExtraCreeksOnCheckChanged(bool newValue)
      //{
      //  ModConfig.GetInstance().SpawnExtraCreeks = newValue;
      //  ModConfig.SaveConfig();
      //}

      //public static void WeatherAffectsWaterSourcesOnCheckChanged(bool newValue)
      //{
      //  ModConfig.GetInstance().WeatherAffectsWaterSources = newValue;
      //  ModConfig.SaveConfig();
      //}

      //public static void ReduceWaterSourcesIntakeOnCheckChanged(bool newValue)
      //{
      //  ModConfig.GetInstance().ReduceWaterSourcesIntake = newValue;
      //  ModConfig.SaveConfig();
      //}

      //public static void AllowFloodsOnCheckChanged(bool newValue)
      //{
      //  ModConfig.GetInstance().AllowFloods = newValue;
      //  ModConfig.SaveConfig();
      //}

      //public static void AllowDroughtsOnCheckChanged(bool newValue)
      //{
      //  ModConfig.GetInstance().AllowDroughts = newValue;
      //  ModConfig.SaveConfig();
      //}

      //public static void FlowingWaterCausesErosionOnCheckChanged(bool newValue)
      //{
      //  ModConfig.GetInstance().FlowingWaterCausesErosion = newValue;
      //  ModConfig.SaveConfig();
      //}

      //public static void ResetWaterSourcesOnButtonClicked()
      //{
      //  if (ImmersiveWaterEngine.GetInstance().IsInitialized == false)
      //    return;
      //  ImmersiveWaterEngine.GetInstance().ResetWaterSourcesToDefaults();
      //}

      //public static void ChooseMapPresetOnButtonClicked()
      //{
      //  if (ImmersiveWaterEngine.GetInstance().IsInitialized == false)
      //    return;

      //  UIView.GetAView().AddUIComponent(typeof(SelectPresetUI)).Show(true);
      //}
    }
  }
}