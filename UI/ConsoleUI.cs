﻿using ColossalFramework;
using ColossalFramework.UI;
using Runaurufu.Common;
using Runaurufu.Utility;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Runaurufu.Console.UI
{
  internal class ConsoleUI : UIPanelAdv
  {
    private ConsoleEngine consoleEngine;

    private UILabel label;

    private UIScrollablePanel scrollablePanel;

    //private UILabel consoleViewLabel;
    private UITextField consoleViewTextField;

    private UITextField consoleInputTextField;

    //private UIDropDown dropDown;

    private UIButton commmitButton;
    private UIButton clearButton;
    private UIButton closeButton;

    public override void Awake()
    {
      this.consoleEngine = ConsoleEngine.GetInstance();

      this.size = new UnityEngine.Vector2(700f, 400f);
      this.anchor = UIAnchorStyle.Bottom;
      this.backgroundSprite = "ButtonMenu";
      this.autoLayoutPadding = new RectOffset(10, 10, 2, 2);
      this.padding = new RectOffset(0, 0, 0, 5);
      this.autoLayout = true;
      this.autoFitChildrenVertically = true;
      this.autoLayoutDirection = LayoutDirection.Vertical;

      this.label = this.AddUIComponent<UILabel>();
      this.label.text = "Console";
      this.label.textAlignment = UIHorizontalAlignment.Center;

      this.scrollablePanel = this.AddUIComponent<UIScrollablePanel>();
      this.scrollablePanel.autoLayout = false;
      this.scrollablePanel.width = this.width - this.autoLayoutPadding.horizontal;
      this.scrollablePanel.height = 400f;
      this.scrollablePanel.clipChildren = true;
      this.scrollablePanel.autoSize = false;
      this.scrollablePanel.verticalScrollbar = new UIScrollbar();
      this.scrollablePanel.wrapLayout = true;
      this.scrollablePanel.maximumSize = this.scrollablePanel.size;
      this.scrollablePanel.color = Color.red;// new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue);

      //this.consoleViewLabel = this.scrollablePanel.AddUIComponent<UILabel>();
      //this.consoleViewLabel.width = this.scrollablePanel.width - this.scrollablePanel.autoLayoutPadding.horizontal;
      //// this.consoleViewLabel.autoHeight = true;
      //this.consoleViewLabel.wordWrap = false;
      //this.consoleViewLabel.textAlignment = UIHorizontalAlignment.Left;
      //this.consoleViewLabel.verticalAlignment = UIVerticalAlignment.Top;
      //this.consoleViewLabel.text = this.consoleEngine.FetchConsoleOutput();
      //this.consoleViewLabel.color = Color.green;

      this.consoleViewTextField = UIFactory.CreateTextField(this.scrollablePanel);
      this.consoleViewTextField.padding = new RectOffset(1, 0, 1, 0);
      // this.consoleViewTextField.anchor = UIAnchorStyle.All;
      this.consoleViewTextField.height = 400f;
      this.consoleViewTextField.autoSize = true;
      this.consoleViewTextField.readOnly = true;
      this.consoleViewTextField.multiline = true;
      this.consoleViewTextField.horizontalAlignment = UIHorizontalAlignment.Left;
      this.consoleViewTextField.verticalAlignment = UIVerticalAlignment.Top;
      this.consoleViewTextField.text = this.consoleEngine.FetchConsoleOutput();
      
      this.consoleInputTextField = UIFactory.CreateTextField(this);
      this.consoleInputTextField.width = this.consoleViewTextField.width;
      //# This must be set to TRUE as otherwise on focus lost every input will be canceled!
      this.consoleInputTextField.submitOnFocusLost = true; 
      this.consoleInputTextField.builtinKeyNavigation = true;
      this.consoleInputTextField.horizontalAlignment = UIHorizontalAlignment.Left;
      this.consoleInputTextField.eventKeyDown += ConsoleInputTextField_eventKeyDown;
      this.consoleInputTextField.eventTextSubmitted += ConsoleInputTextField_eventTextSubmitted;
      
      //this.dropDown = UIFactory.CreateDropDown(this);

      //this.dropDown.autoListWidth = true;
      //this.dropDown.selectedIndex // try to find preset using found water sources :o

      this.commmitButton = UIFactory.CreateButton(this);
      this.commmitButton.text = "Commit";
      this.commmitButton.textHorizontalAlignment = UIHorizontalAlignment.Center;
      this.commmitButton.textVerticalAlignment = UIVerticalAlignment.Middle;
      this.commmitButton.eventClick += CommitButton_eventClick;

      this.clearButton = UIFactory.CreateButton(this);
      this.clearButton.text = "Clear";
      this.clearButton.textHorizontalAlignment = UIHorizontalAlignment.Center;
      this.clearButton.textVerticalAlignment = UIVerticalAlignment.Middle;
      this.clearButton.eventClick += ClearButton_eventClick;

      this.closeButton = UIFactory.CreateButton(this);
      this.closeButton.text = "Close";
      this.closeButton.textHorizontalAlignment = UIHorizontalAlignment.Center;
      this.closeButton.textVerticalAlignment = UIVerticalAlignment.Middle;
      this.closeButton.eventClick += CloseButton_eventClick;

      base.Awake();
    }

    private bool wasEnterSubmitted = false;
    
    private void ConsoleInputTextField_eventTextSubmitted(UIComponent component, string value)
    {
      if (this.wasEnterSubmitted)
      {
        wasEnterSubmitted = false;
        this.consoleInputTextField.Focus();
      }
    }

    private void ConsoleInputTextField_eventKeyDown(UIComponent component, UIKeyEventParameter eventParam)
    {
      if (eventParam.keycode == KeyCode.Return || eventParam.keycode == KeyCode.KeypadEnter)
      {
        this.CommitInput();
        wasEnterSubmitted = true;
        return;
      }
    }

    private void CommitButton_eventClick(UIComponent component, UIMouseEventParameter eventParam)
    {
      this.CommitInput();
    }

    private void ClearButton_eventClick(UIComponent component, UIMouseEventParameter eventParam)
    {
      this.consoleEngine.ClearConsoleOutput();
    }

    private void CloseButton_eventClick(UIComponent component, UIMouseEventParameter eventParam)
    {
      this.Hide();
    }
    
    private void CommitInput()
    {
      string input = this.consoleInputTextField.text;

      this.consoleInputTextField.text = string.Empty;

      this.consoleEngine.CommitToConsole(input);
    }

    public new void Update()
    {
      if (this.IsUpdateAllowed)
      {
        if (this.consoleEngine.ConsoleOutputChangedSinceLastFetch)
        {
          this.consoleViewTextField.text = this.consoleEngine.FetchConsoleOutput();
          this.consoleViewTextField.MoveToEnd();
          this.scrollablePanel.FitToContents();
          this.scrollablePanel.ScrollToBottom();
        }
      }
      base.Update();
    }
  }
}