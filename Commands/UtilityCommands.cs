﻿using ColossalFramework;
using Runaurufu.Common;
using Runaurufu.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Runaurufu.Console.Commands
{
  /// <summary>
  /// Set of utility commands available for console.
  /// </summary>
  static class UtilityCommands
  {
    /// <summary>
    /// Clear console output.
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    [ConsoleCommandAttribute("clear")]
    private static void ClearConsoleOutput(string[] args)
    {
      ConsoleEngine.GetInstance().ClearConsoleOutput();
    }

    /// <summary>
    /// Print text into console output.
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    [ConsoleCommandAttribute("echo")]
    private static void PrintToConsoleOutput(string[] args)
    {
      ConsoleEngine.GetInstance().WriteToConsoleOutput(string.Join(" ", args));
    }

    /// <summary>
    /// Print text into console output and put line break afterwards.
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    [ConsoleCommandAttribute("print")]
    private static void PrintToConsoleOutputWithLineBreak(string[] args)
    {
      ConsoleEngine.GetInstance().WriteToConsoleOutput(string.Join(" ", args) + Environment.NewLine);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    [ConsoleCommandAttribute("set", "time")]
    private static void SetTime(string[] args)
    {
      int legth = args.Length;

      float value;

      if (legth == 0)
      {
        value = 0;
      }
      else
      {
        if (legth > 1)
        {
          // try to parse from hh:mm:ss
          float hh = 0;
          float mm = 0;
          float ss = 0;


          if (float.TryParse(args[0], out hh))
          {
            if (legth >= 2 && float.TryParse(args[1], out mm))
            {
              if (legth >= 3)
                float.TryParse(args[2], out ss);
            }
          }

          if (hh < 0 || hh > 23 || mm < 0 || mm > 59 || ss < 0 || ss > 59)
            value = -1;
          else
            value = hh + mm / 60 + ss / 3600;
        }
        else
        {
          DateTime dateTime;
          if (DateTime.TryParse(args[0], out dateTime))
          {
            value = dateTime.Hour + dateTime.Minute / 60f + dateTime.Second / 3600f;
          }
          else if (float.TryParse(args[0], out value) == false)
          {
            value = -1;
          }
        }
      }

      if (value >= 0.0f && value < 24.0f)
      {
        SimulationManager simulationManager = SimulationManager.instance;

        int num = (int)((value - simulationManager.m_currentDayTimeHour) / SimulationManager.DAYTIME_FRAME_TO_HOUR);
        simulationManager.m_dayTimeOffsetFrames = (uint)(((ulong)simulationManager.m_dayTimeOffsetFrames + (ulong)((long)num)) % (ulong)SimulationManager.DAYTIME_FRAMES);

        simulationManager.m_currentDayTimeHour = value;
      }
      else
      {
        ConsoleEngine.GetInstance().WriteToConsoleOutput("Unable to parse command arguments into meaningful time string");
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    [ConsoleCommandAttribute("add", "money")]
    private static void AddMoney(string[] args)
    {
      EconomyManager eco = EconomyManager.instance;
      if (eco != null && args.Length > 0)
      {
        long value;
        if (long.TryParse(args[0], out value))
        {
          //eco.SetFieldValue("m_cashAmount", eco.InternalCashAmount + value);
          // this.m_lastCashAmount = this.m_EconomyWrapper.OnUpdateMoneyAmount(this.m_lastCashAmount + (long)amount);

          eco.AddResource(EconomyManager.Resource.RewardAmount,
            (int)value,
            ItemClass.Service.None,
            ItemClass.SubService.None,
            ItemClass.Level.None);

          ConsoleEngine.GetInstance().WriteToConsoleOutput("Money added: {0}", value);
        }
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    [ConsoleCommandAttribute("set", "money")]
    private static void SetMoney(string[] args)
    {
      EconomyManager eco = EconomyManager.instance;
      if (eco != null && args.Length > 0)
      {
        long value;
        if (long.TryParse(args[0], out value))
        {
          //eco.SetFieldValue("m_cashAmount", value);
          // this.m_lastCashAmount = this.m_EconomyWrapper.OnUpdateMoneyAmount(this.m_lastCashAmount + (long)amount);

          eco.AddResource(EconomyManager.Resource.RewardAmount,
            (int)(value - eco.InternalCashAmount),
            ItemClass.Service.None,
            ItemClass.SubService.None,
            ItemClass.Level.None);

          ConsoleEngine.GetInstance().WriteToConsoleOutput("Money set to: {0}", value);
        }
      }
    }
  }
}
