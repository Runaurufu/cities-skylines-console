﻿using System;
using ICities;
using Runaurufu.Utility;
using UnityEngine;
using ColossalFramework.UI;
using Runaurufu.Console.UI;

namespace Runaurufu.Console
{
  public class ConsoleWaterThreading : ThreadingExtensionBase
  {
    public override void OnCreated(IThreading threading)
    {
      base.OnCreated(threading);

      //ClimateControlEngine.GetInstance().ThreadingManager = threading;
    }

    public override void OnUpdate(float realTimeDelta, float simulationTimeDelta)
    {
      base.OnUpdate(realTimeDelta, simulationTimeDelta);

      try
      {
        if ((Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.LeftControl)
            || Input.GetKey(KeyCode.RightShift) && Input.GetKey(KeyCode.RightControl))
          && Input.GetKey(KeyCode.BackQuote))
        {
          ConsoleEngine.GetInstance().ShowConsole();
        }
      }
      catch (Exception ex)
      {
        Utility.Logger.Log("ConsoleEngine.GetInstance().ShowConsole(); Exception: " + Environment.NewLine + ex.ToString());
      }
    }


  }
}