﻿using ColossalFramework;
using ColossalFramework.UI;
using ICities;
using Runaurufu.Console.UI;
using Runaurufu.Utility;
using System;
using UnityEngine;

namespace Runaurufu.Console
{
  public class ConsoleLoading : LoadingExtensionBase
  {
    public override void OnLevelLoaded(LoadMode mode)
    {
      base.OnLevelLoaded(mode);

      try
      {
        ModConfig.LoadConfig();
      }
      catch (Exception ex)
      {
        Utility.Logger.Log("ModConfig.LoadConfig() Exception: " + Environment.NewLine + ex.ToString());
      }

      if (mode != LoadMode.LoadGame && mode != LoadMode.NewGame)
        return;

      // Scan for commands in assemblies!
      ConsoleEngine.GetInstance().ScanForCommands();
    }

    public override void OnLevelUnloading()
    {
      base.OnLevelUnloading();
    }
  }
}