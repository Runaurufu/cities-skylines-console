﻿using ColossalFramework.UI;
using Runaurufu.Common;
using Runaurufu.Console.UI;
using Runaurufu.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Runaurufu.Console
{
  class ConsoleEngine : IConsole
  {
    private object consoleSync = new object();

    private ConsoleUI console = null;

    public void ShowConsole()
    {
      lock (this.consoleSync)
      {
        if (this.console != null && this.console.isEnabled == false)
        {
          if (this.console.isVisible)
            this.console.Hide();
          this.console = null;
        }

        if (this.console == null)
          this.console = UIView.GetAView().AddUIComponent(typeof(ConsoleUI)) as ConsoleUI;

        this.console.Show(true);
      }
    }

    #region GetInstance
    private static object syncRoot = new object();
    private volatile static ConsoleEngine instance = null;
    public static ConsoleEngine GetInstance()
    {
      if (instance == null)
      {
        lock (syncRoot)
        {
          if (instance == null)
            instance = new ConsoleEngine();
        }
      }

      return instance;
    }


    #endregion

    private object consoleOutputSync = new object();
    private volatile bool consoleOutputChanged = false;
    private StringBuilder consoleOutput = new StringBuilder();

    internal String ConsoleOutput
    {
      get
      {
        lock (this.consoleOutputSync)
        {
          return this.consoleOutput.ToString();
        }
      }
    }

    internal String FetchConsoleOutput()
    {
      lock (this.consoleOutputSync)
      {
        this.consoleOutputChanged = false;
        return this.consoleOutput.ToString();
      }
    }

    internal void PushToConsoleOutput(string text, params object[] args)
    {
      lock (this.consoleOutputSync)
      {
        this.consoleOutput.AppendFormat(text, args);
        this.consoleOutputChanged = true;
      }
    }

    internal bool ConsoleOutputChangedSinceLastFetch
    {
      get
      {
        return consoleOutputChanged;
      }
    }

    private volatile object handlersSync = new object();
    private List<CommanFallbackHandler> fallbackHandlers = new List<CommanFallbackHandler>();
    private Dictionary<string, object> commandHandlers = new Dictionary<string, object>();

    private string[] GetSeparateArguments(string inputPath)
    {
      return inputPath.Split(' ');
    }

    /// <summary>
    /// This method will scan all loaded assemblies for commands to register into ConsoleEngine.
    /// </summary>
    public void ScanForCommands()
    {
      Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
      foreach (Assembly assembly in assemblies)
      {
        this.ScanForCommands(assembly);
      }
    }

    private void ScanForCommands(Assembly assembly)
    {
      Type[] types = assembly.GetTypes();
      foreach (Type type in types)
      {
        this.ScanForCommands(type);
      }
    }

    private void ScanForCommands(Type type)
    {
      MethodInfo[] staticMethods = type.GetMethods(BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public);
      foreach (MethodInfo methodInfo in staticMethods)
      {
        ConsoleCommandAttribute[] attributes = methodInfo.GetCustomAttributes(typeof(ConsoleCommandAttribute), false).OfType<ConsoleCommandAttribute>().ToArray();
        if (attributes.Length == 0)
          continue;

        CommandHandler handler = Delegate.CreateDelegate(typeof(CommandHandler), methodInfo) as CommandHandler;
        if (handler != null)
        {
          foreach (ConsoleCommandAttribute attr in attributes)
          {
            this.RegisterCommand(attr.CommandPath, handler);
          }
        }
      }
    }

    public bool RegisterCommand(string inputPath, CommandHandler handler)
    {
      if (handler == null)
        return false;

      if (inputPath == null)
        return false;

      string[] splitPath = this.GetSeparateArguments(inputPath);

      if (splitPath.Length == 0)
        return false;

      return RegisterCommand(splitPath, handler);
    }

    public bool RegisterCommand(string[] inputPath, CommandHandler handler)
    {
      if (inputPath == null || inputPath.Length == 0)
        return false;

      if (handler == null)
        return false;

      lock (this.handlersSync)
      {

        Dictionary<string, object> dict = this.commandHandlers;

        for (int i = 0; i < inputPath.Length; i++)
        {
          string currentLevelPath = inputPath[i];

          if (i == inputPath.Length - 1)
          { // this is last level traversal
            if (dict.ContainsKey(currentLevelPath))
            {
              if (dict[currentLevelPath] is Dictionary<string, object>
                && ((Dictionary<string, object>)dict[currentLevelPath]).Count == 0)
              {
                // There is dictionary but as it have no entries we can safety remove it!
                dict[currentLevelPath] = null;
              }
              else
              {
                return false; // There is something already registered!
              }
            }

            // We can register handler!
            dict[currentLevelPath] = handler;

            return true;
          }
          else
          {
            if (dict.ContainsKey(currentLevelPath))
            {
              if (dict[currentLevelPath] is Dictionary<string, object>)
              {
                dict = dict[currentLevelPath] as Dictionary<string, object>;
              }
              else
              {
                return false;
              }
            }
            else
            {
              dict[currentLevelPath] = new Dictionary<string, object>();
              dict = dict[currentLevelPath] as Dictionary<string, object>;
            }
          }
        }
      }

      return false;
    }

    public bool RegisterNotHandledInputFallback(CommanFallbackHandler handler)
    {
      if (handler == null)
        return false;

      lock (this.handlersSync)
        fallbackHandlers.Add(handler);

      return true;
    }

    public void WriteToConsoleOutput(string textToWrite, params object[] arguments)
    {
      this.PushToConsoleOutput(textToWrite, arguments);
    }

    public void ClearConsoleOutput()
    {
      lock (this.consoleOutputSync)
      {
        this.consoleOutput.Remove(0, this.consoleOutput.Length);
        this.consoleOutputChanged = true;
      }
    }

    public void CommitToConsole(string textToCommit, params object[] arguments)
    {
      string consoleInput = null;
      try
      {
        consoleInput = string.Format(textToCommit, arguments);
      }
      catch (Exception ex)
      {
        this.PushToConsoleOutput(Environment.NewLine
          + "!! Exception thrown during console input parsing !!"
          + Environment.NewLine
          + ex.Message);
        return;
      }

      this.HandleConsoleInput(consoleInput);
    }

    private void HandleConsoleInput(string input)
    {
      if (string.IsNullOrEmpty(input))
        return;

      string[] args = GetSeparateArguments(input);

      lock (this.handlersSync)
      {
        Dictionary<string, object> dict = this.commandHandlers;
        for (int i = 0; i < args.Length; i++)
        {
          if (dict.ContainsKey(args[i]))
          {
            if (dict[args[i]] is CommandHandler)
            {
              // Use that commandHandler
              CommandHandler handler = (CommandHandler)dict[args[i]];


              string[] commandArgs = args.Skip(i + 1).ToArray();

              handler(commandArgs);
              // we found what we wanted so we can exit.
              return;

            }
            else
            {
              dict = (Dictionary<string, object>)dict[args[i]];
            }
          }
          else
          {
            // We did not found that path in registered commands!
            break;
          }
        }

        foreach (CommanFallbackHandler fallbackHandler in this.fallbackHandlers)
        {
          if (fallbackHandler(args))
            return;
        }
      }

      this.PushToConsoleOutput(Environment.NewLine
        + "Unhandled console input: {0}", input);
    }
  }
}
