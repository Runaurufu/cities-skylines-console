﻿using Runaurufu.Utility;
using System;
using System.IO;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;

namespace Runaurufu.Console
{
  /// <summary>
  /// 
  /// </summary>
  [Serializable]
  public class ModConfig //: IXmlSerializable
  {
    private static ModConfig instance;

    public static ModConfig GetInstance()
    {
      if (instance == null)
        LoadConfig();

      if (instance == null)
        instance = new ModConfig();

      return instance;
    }

    public ModConfig()
    {
      //this.UseClimateControlWaterQuantities = false;
      //this.DynamicWaterSourcesPlacement = false;
      //this.SpawnExtraCreeks = false;
      //this.WeatherAffectsWaterSources = false;
      //this.FlowingWaterCausesErosion = false;
      //this.ReduceWaterSourcesIntake = false;
      //this.AllowFloods = false;
      //this.AllowDroughts = false;
    }

    ///// <summary>
    ///// Determines should Immersive Water use available water quantities from Climate Control if it is available.
    ///// </summary>
    //public bool UseClimateControlWaterQuantities { get; set; }

    ///// <summary>
    ///// Determines should water sources be dynamically created.
    ///// </summary>
    //public bool DynamicWaterSourcesPlacement { get; set; }

    ///// <summary>
    ///// Determines should there be placed additional water sources on the map acting like a creeks..
    ///// </summary>
    //public bool SpawnExtraCreeks { get; set; }

    ///// <summary>
    ///// Determines should water sources respond to changes in weather.
    ///// </summary>
    //public bool WeatherAffectsWaterSources { get; set; }

    ///// <summary>
    ///// Determines should flowing water causes terrain erosion.
    ///// </summary>
    //public bool FlowingWaterCausesErosion { get; set; }

    ///// <summary>
    ///// Heavily reduce water sources intake capacity.
    ///// </summary>
    //public bool ReduceWaterSourcesIntake { get; set; }

    ///// <summary>
    ///// Allow floods.
    ///// </summary>
    //public bool AllowFloods { get; set; }

    ///// <summary>
    ///// Allow droughts.
    ///// </summary>
    //public bool AllowDroughts { get; set; }


    public static void LoadConfig()
    {
      if (File.Exists(AssemblyConfig.GetConfigFileName()) == false)
        return;

      try
      {
        XmlSerializer serializer = new XmlSerializer(typeof(ModConfig));
        using (XmlReader xmlReader = XmlReader.Create(AssemblyConfig.GetConfigFileName()))
          instance = (ModConfig)serializer.Deserialize(xmlReader);
      }
      catch (Exception ex)
      {
        instance = new ModConfig();
      }
    }

    public static bool SaveConfig()
    {
      if (instance == null)
        return false;

      try
      {
        XmlSerializer serializer = new XmlSerializer(typeof(ModConfig));
        using (XmlWriter xmlWriter = XmlWriter.Create(AssemblyConfig.GetConfigFileName()))
          serializer.Serialize(xmlWriter, instance);
      }
      catch (Exception ex)
      {
        return false;
      }
      return true;
    }
  }
}